= how to release to npm
:version: 0.0.7
:repo: asciidoctor-mathjax.js
:bundle: djencks-asciidoctor-mathjax
:url: https://gitlab.com/djencks/{repo}/-/jobs/artifacts/master/raw/{bundle}-v{version}.tgz?job=bundle-stable


* update this file, package.json, and the two README files with the new version.
* merge to master at gitlab
* When the CI completes the bundle to release should be at:
{url}

* run
npm publish --access public {url} --dry-run

* run
npm publish --access public {url}
